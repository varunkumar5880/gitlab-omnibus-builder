# Install gitlab-ci-multi-runner so that we can trigger omnibus builds from GitLab CI.

pkg_url = 'https://packages.gitlab.com'
pkg_repo = 'runner/gitlab-ci-multi-runner'
pkg_name = 'gitlab-ci-multi-runner'

package 'curl'

case node['platform_family']
when 'debian'
  execute "add #{pkg_url}/#{pkg_repo} apt repo" do
    command "curl #{pkg_url}/install/repositories/#{pkg_repo}/script.deb.sh | bash"
    creates "/etc/apt/sources.list.d/#{pkg_repo.sub('/','_')}.list"
  end

  package pkg_name
when 'rhel'
  execute "add #{pkg_url}/#{pkg_repo} yum repo" do
    command "curl #{pkg_url}/install/repositories/#{pkg_repo}/script.rpm.sh | bash"
    creates "/etc/yum.repos.d/#{pkg_repo.sub('/','_')}.repo"
  end

  package pkg_name
end
